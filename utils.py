from operator import itemgetter
import csv
import datetime


def format_response(data):
    """
    Function formats each row and sorts it based on the reversed monarch's name. Rather than reading in the file after
    the context manager used to write to it is closed, the function uses data presently stored in the formatted_rows
    property to generate the response using generator compreshion.
    :param data: dictionary representing json response
    :return: string representing the csv file
    """
    formatted_rows = [['Name', 'Country', 'House', 'Year of Coronation', 'Ingestion Time']]

    for row in data:
        if 'Wessex' not in row.get('hse'):
            formatted_row = format_row(row)
            formatted_rows.append(formatted_row)

    sorted_formatted_rows = sorted(formatted_rows, key=itemgetter(0))

    with open('kings.csv', mode='w') as kings_file:
        king_writer = csv.writer(kings_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in sorted_formatted_rows:
            king_writer.writerow(row)

    return '\n'.join(','.join(row) for row in sorted_formatted_rows)


def format_row(row):
    """
    Formats each row and returns list containing attributes
    :param row: dictionary representing json response
    :return: list
    """
    name = row.get('nm').split(' ')[0][::-1]
    country = "".join(e[0] for e in row.get('cty').split())
    house = row.get('hse')
    coronation = row.get('yrs', '').split('-')[0]
    timestamp = str(datetime.datetime.utcnow())
    return [name, country, house, coronation, timestamp]
