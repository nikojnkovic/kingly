# Kingly Assignment

## Instructions

Install requirements
```commandline
pip install -r requirements.txt
```

Run application
```commandline
flask run
```

Target the api endpoint either through the command line or browser
```commandline
curl -X GET http://127.0.0.1:5000/kings
```

## Approach & Improvements
The assignment calls for the retrival of json data on each request, storing the data in a csv file and returning a csv
as a response. This application does this but the logic and workflow could be significantly improved. Since the csv file
is not referenced after being created there is little reason to create it. An improvement might include retrieving the
data and writing it to file before the first request using `@before_first_request` decorator. Or to treat the csv file
as a cache and have it expire based on the timestamp attribute in the csv file. Other improvements include adding tests,
better exception handing with custom exceptions, and using an application factory for easier testing.