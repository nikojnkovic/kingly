from flask import Flask, make_response
from utils import format_response
import requests

app = Flask(__name__)
URL = 'http://mysafeinfo.com/api/data?list=englishmonarchs&format=json'


@app.route('/kings', methods=['GET'])
def get_kings():
    """
    Function make request to third party api, passes the json response to retrieve_kings(), configures response
    headers for a csv file, and returns the response from retrieve_kings()
    :return: make_response object
    """
    try:
        data = requests.get(URL)
        formatted_csv = format_response(data.json())
        response = make_response(formatted_csv)
        response.headers["Content-Disposition"] = "attachment; filename=export.csv"
        response.headers["Content-type"] = "text/csv"
        return response
    except Exception:
        return 'Unable to process request', 500


if __name__ == '__main__':
    app.run()
